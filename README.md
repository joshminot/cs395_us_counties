# CS-395 Challenge 1 --- US Counties

Scripts for generating interactive maps related to US quality of life. 

Inspired by [this blog post](https://www.nytimes.com/2014/06/26/upshot/where-are-the-hardest-places-to-live-in-the-us.html) 
by the Upshot in the NYTimes.

## Setup 

We assume `anaconda` is installed for setup.

Run the following to install the dependencies in a conda environment for the project:

`conda env create -f requirements.yml` 

Activate the env:

`conda activate us_counties_cs395_3.6`

#### IMPORTANT!

Run `config.py <census API key>` from the main directory in order to create default data, output, and key directories. API is optional
but will be required for API keys. 

Paste your US Census API key in `.keys/census_keys.json` as `{'APIKEY':<API key>}` or rerun `config.py <census API key>` to add keys after initializing. 




## Project structure

``` 
|-- notebooks (for jupyter notebooks)
|-- data (intermediate output, and original upshot data (do not git add other files))
|-- src (python scripts)
    |-- utils.py (utility functions, e.g. load, plot)
    |-- interactive.py (runner for interactive plots)
    |-- consts.py (constant values, e.g. colors, labels, etc.)
    |-- cli.py  (command line interface support)
|-- requirements.txt (requirements file)
|-- config.py (configure directories and API key)
|-- .keys
    |-- census_keys.json (census keys for API)
```

## Example
To generate a basic version of the Upshot interactive plot:

`python3 interactive.py interactive /data/CountyData.tsv` 

For US Census API:

`python interactive.py censusapi --year 2016 --tables 'B01001_003E'`

 