from pathlib import Path
import json
import sys

# check for API key
try:
    apikey = sys.argv[1]
except IndexError:
    apikey =None
    print('No API Key Passed... Initializing without Census API support')


# make directories for use later on
cwd = Path.cwd()

paths = {'plots':cwd/'plots',
         'data':cwd/'data',
         'keys':cwd/'.keys'}

# make directories
for key, val in paths.items():
    val.mkdir(parents=False,
              exist_ok=True)
    # write keys to key file
    if key =='keys' and apikey:
        json.dump({'APIKEY': apikey}, open(paths['keys'] / 'census_keys.json', 'w'))
    paths[key] = str(val)



json.dump(paths, open('src/consts.json', 'w'))