import sys
import csv
import json
import pandas as pd
import urllib.request
from us import states
from pathlib import Path
from census import Census


def create_table(key, normalize, year, tables):
    """
    Returns the categories specified by year / table.

    Parameters:
        :str key:			API key.
        :bool normalize:	Whether the data should be normalized by population.
        :int year:			The year we're retrieving data for.
        :list tables:		Thet tables we're observing data for.

    Returns:
        None

    Sample call:
        data, metadata = create_table('ae97d75e1af15025197a396ac162131b75ece39b', True, 2010, ['B01001_003E','B01001_004E'])3
    """

    # Always consider population as first attribute.
    tables = ['B01001_001E'] + tables

    # Get data
    c = Census(key, year)
    data = c.acs5.get(tables, geo={'for': 'county:*', 'in': 'state:*'})

    # Turn to dataframe and clean
    data = pd.DataFrame(data)
    data['FIPS'] = data['state'] + data['county']
    del data['state']
    del data['county']

    # Convert all columns to numeric:
    for i in data.keys():
        data[i] = pd.to_numeric(data[i])

    # If normalizing, divide by population.
    if (normalize):
        for i in data.keys():
            if (i != 'FIPS' and i != tables[0]):
                data[i] = round(data[i] / data[tables[0]], 4)

    return data, get_metadata(year, tables)


def get_metadata(year, tables):
    """
    Finds the description of a table.

    Parameters:
        :int year: 		The year, available 2010-2017
        :list tables:	The table names.

    Returns;
        :dict tables: 	Mapping of table names to descriptions.
    """

    metadata = {}.fromkeys(tables)

    for i in tables:
        url = 'https://api.census.gov/data/{}/acs/acs5/groups/{}.json'.format(str(year), i.split('_')[0])
        url = urllib.request.urlopen(url)
        data = json.loads(url.read().decode())

        metadata[i] = data['variables'][i]['label'].replace('!!', ' ')

    return metadata


def get_census_data(apikey,tables, year, normalize):
    """
    runner for

    TODO: pass save location for use in plotting script
    """
    return create_table(apikey, normalize, year, tables)
