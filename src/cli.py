import argparse
from argparse import ArgumentDefaultsHelpFormatter
from operator import attrgetter


class SortedMenu(ArgumentDefaultsHelpFormatter):
    def add_arguments(self, actions):
        actions = sorted(actions, key=attrgetter('option_strings'))
        super(SortedMenu, self).add_arguments(actions)


def parser():
    """ Util function to parse command-line arguments """

    return argparse.ArgumentParser(
        formatter_class=SortedMenu,
        description='Interactive plots of US county data'
    )

