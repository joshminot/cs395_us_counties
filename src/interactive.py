import plotly
import pandas as pd
import numpy as np
import argparse
import cli
import utils
import json
from pathlib import Path


def parse_args(args, consts, apikey):
    parser = cli.parser()

    # optional subparsers
    subparsers = parser.add_subparsers(help='Arguments for specific action. Mostly interactive plotting for Upshot '
                                            'project', dest='dtype')
    subparsers.required = True

    interactive_parser = subparsers.add_parser(
        'upshot',
        help='make an interactive map'
    )
    #interactive_parser.add_argument(
     #   'datapath',
        #required=False,
    #    default=Path(consts['data'])/'CountyData.tsv',
   #     help='path to a data'
   # )

    test_parser = subparsers.add_parser(
        'test',
        help='make an interactive map'
    )


    census_parser_flat = subparsers.add_parser(
        'census',
        help='make an interactive map'
    )

    census_parser_flat.add_argument(
        'datapath',
        help='Path to data'
    )

    #  ------------------------------ Census API Parser ---------------------- #

    census_parser = subparsers.add_parser(
        'censusapi',
        help='make an interactive map'
    )

    census_parser.add_argument(
        '-t',
        '--tables',
        nargs='+',
        help='tables to pull from census'
    )
    census_parser.add_argument(
        '-y',
        '--year',
        help='year to request for census',
        type=int
    )
    census_parser.add_argument(
        '-n',
        '--normalize',
        help='normalize value by county population',
        action='store_true'
    )
    census_parser.add_argument(
        '-k',
        '--keyapi',
        help='API Key for US Census',
        default = apikey
    )

    parser.add_argument(
        '-o', '--outdir',
        default=Path(consts['plots']),
        help='absolute Path to save figures',
        type=Path
    )

    parser.add_argument(
        '-d', '--data',
        default=Path(consts['data']),
        help='absolute Path to save figures',
        type=Path
    )

    parser.add_argument(
        '--grid',
        help='make the grid version of a figure',
        action='store_true'
    )
    parser.add_argument(
        '--noplot',
        help='do not plot',
        action='store_true'
    )

    return parser.parse_args(args)


def main(args=None):
    # Load consts / default paths
    consts = json.load(open('consts.json', 'r'))
    # Load API Key
    try:
        apikey = json.load(open(Path(consts['keys'])/'census_keys.json','r'))['APIKEY']
    except FileNotFoundError:
        apikey=None
        print('Warning: No api key found')

    # Load the rest of args
    args = parse_args(args, consts, apikey)

    if args.dtype == 'test':
        utils.plotly_test()

    if args.dtype == 'upshot':
        if args.grid:
            utils.plotly_upshot_grid(args.data/'CountyData.tsv', args.outdir)
        else:
            utils.plotly_upshot(args.data/'CountyData.tsv', args.outdir)

    if args.dtype == 'census':
        utils.plotly_us_census(args.datapath)

    if args.dtype == 'censusapi':
        if args.noplot:
            utils.get_census_ts(args.keyapi, args.tables, args.year, args.normalize, args.data, args.outdir)
        else:
            utils.plotly_us_census_api(args.keyapi, args.tables, args.year, args.normalize, args.data, args.outdir)


if __name__ == '__main__':
    main()
