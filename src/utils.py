import plotly
import pandas as pd
import numpy as np
import argparse
import plotly.figure_factory as ff
from pathlib import Path
import plotly.express as px
import geojson
from urllib.request import urlopen
import json
import census_api

from plotly.subplots import make_subplots
import plotly.graph_objs as go
from pprint import pprint


def read_df(dpath, clean_numeric=False):
    """
    read and clean counties df
    :param dpath: path to upshot counties data
    :return: pandas dataframe
    """

    df = pd.read_csv(dpath, sep='\t', encoding='utf8')
    if clean_numeric:
        target_cols = df.columns[2:]
        df[target_cols] = df[target_cols].applymap(lambda x: pd.to_numeric(x, errors='coerce'))

        df.dropna(inplace=True)

    df['id'] = df['id'].apply(lambda x: str(x).zfill(5))  # zero pad ids

    return df


def read_census_xls(dpath, loc_id='STCOU'):
    """
    :param dpath: path to xls file
    :return: pandas dataframe
    """
    df = pd.read_excel(dpath)

    df[loc_id] = df[loc_id].apply(lambda x: str(x).zfill(5))  # zero pad ids

    return df


def rank_upshot(upshot_df):
    """
    Rank columns in upshot dataset
    """

    #upshot_df[upshot_df.columns[2:]]
    ascending = {
    'education':False,
    'income':False,
    'unemployment':True,
    'disability':True,
    'life':False,
    'obesity':True}

    mod_keys = []
    for key, val in ascending.items():
        mod_key = f'{key}_rank'
        upshot_df[mod_key] = upshot_df[key].rank(ascending=val)
        mod_keys.append(mod_key)

    return upshot_df, mod_keys






# ----------------------------- Plotting Functions --------------------------------------------------------#


def plotly_test_deprecated():
    """
    simple plotly test from https://plot.ly/python/county-choropleth/

    run to make sure geo-plolty is properly configured
    """
    print('Running test')
    df_sample = pd.read_csv('https://raw.githubusercontent.com/plotly/datasets/master/laucnty16.csv')
    df_sample['State FIPS Code'] = df_sample['State FIPS Code'].apply(lambda x: str(x).zfill(2))
    df_sample['County FIPS Code'] = df_sample['County FIPS Code'].apply(lambda x: str(x).zfill(3))
    df_sample['FIPS'] = df_sample['State FIPS Code'] + df_sample['County FIPS Code']

    colorscale = ["#f7fbff", "#ebf3fb", "#deebf7", "#d2e3f3", "#c6dbef", "#b3d2e9", "#9ecae1",
                  "#85bcdb", "#6baed6", "#57a0ce", "#4292c6", "#3082be", "#2171b5", "#1361a9",
                  "#08519c", "#0b4083", "#08306b"]
    endpts = list(np.linspace(1, 12, len(colorscale) - 1))
    fips = df_sample['FIPS'].tolist()
    values = df_sample['Unemployment Rate (%)'].tolist()

    fig = ff.create_choropleth(
        fips=fips, values=values,
        binning_endpoints=endpts,
        colorscale=colorscale,
        show_state_data=True,
        show_hover=True, centroid_marker={'opacity': 0},
        asp=2.9, title='USA by Unemployment %',
        legend_title='% unemployed'
    )
    plotly.offline.plot(fig, filename='deprecated_test.html')

    fig.layout.template = None

    fig.show()


def plotly_test():
    """
        simple plotly test from https://plot.ly/python/county-choropleth/

        run to make sure geo-plolty is properly configured
    """

    with urlopen('https://raw.githubusercontent.com/plotly/datasets/master/geojson-counties-fips.json') as response:
        counties = json.load(response)

    df = pd.read_csv("https://raw.githubusercontent.com/plotly/datasets/master/fips-unemp-16.csv",
                     dtype={"fips": str})

    fig = px.choropleth(df, geojson=counties, locations='fips', color='unemp',
                        color_continuous_scale="Viridis",
                        range_color=(0, 12),
                        scope="usa",
                        labels={'unemp': 'unemployment rate'}
                        )
    fig.update_layout(margin={"r": 0, "t": 0, "l": 0, "b": 0})
    plotly.offline.plot(fig, filename='test.html')
    fig.show()


def plotly_upshot(dpath, outpath):
    """
        simple plotly test from https://plot.ly/python/county-choropleth/

        basic upshot
        :param dpath: path to the upshot countydata file
    """

    print('Running upshot gen.')
    with urlopen('https://raw.githubusercontent.com/plotly/datasets/master/geojson-counties-fips.json') as response:
        counties = json.load(response)

    df = read_df(dpath, clean_numeric=True)
    print(df.head())

    fig = px.choropleth(df, geojson=counties, locations='id', color='rank',
                        color_continuous_scale=['#367B7F', '#EBE3D7', '#F28124'],
                        scope="usa",
                        hover_name=df['County'],
                        hover_data=df[df.columns[2:]],
                        )
    fig.update_traces(
        marker_line_color='white'
    )
    fig.update_layout(margin={"r": 0, "t": 0, "l": 0, "b": 0})
    plotly.offline.plot(fig, filename=str(outpath / 'upshot_test.html'))
    fig.show()


def plotly_upshot_grid(dpath, save_loc):
    """
        simple plotly test from https://plot.ly/python/county-choropleth/

        Upshot data plotted as small multiples
        :param dpath: path to the upshot countydata file\
        :param saveloc: path to save the html

        TODO add rank functions
    """

    print('Running upshot plot with grid.')
    with urlopen('https://raw.githubusercontent.com/plotly/datasets/master/geojson-counties-fips.json') as response:
        counties = json.load(response)

    df = read_df(dpath, clean_numeric=True)
    print(df.head())

    nrows = 3
    ncols = 2

    target_cols = df.columns[2:]

    upshot_ranks, org_names = rank_upshot(df)

    print(upshot_ranks.head())

    #exit()
    target_cols=org_names

    panel = make_subplots(
        rows=nrows, cols=ncols,
        # column_widths=[0.5, 0.5],
        # row_heights=[0.3]*3,
        specs=[[{"type": "choropleth"}, {"type": "choropleth"}],
               [{"type": "choropleth"}, {"type": "choropleth"}],
               [{"type": "choropleth"}, {"type": "choropleth"}]],
        subplot_titles=[t.replace('_', ' ').title() for t in target_cols],
        horizontal_spacing=0,
        vertical_spacing=0.05)

    coli = 0
    for i in range(1, nrows + 1):
        for j in range(1, ncols + 1):
            print(i, j)
            col = target_cols[coli]

            maps = px.choropleth(df, geojson=counties, locations='id', color=col,
                                 color_continuous_scale="Viridis_r",
                                 range_color=(0, 12),
                                 scope="usa",
                                 hover_data=df,#[target_cols] ,
                                 hover_name=df['County'],
                                 )
            panel.add_trace(go.Choropleth(maps._data[0]), col=j, row=i)

            coli += 1

    panel.update_traces(
        marker_line_color='white',
        marker_line_width=.25
    )

    panel.update_geos(scope='usa')
    panel.update_layout(margin={"r": 0, "t": 40, "l": 0, "b": 0})

    plotly.offline.plot(panel, filename=str(save_loc / 'upshot_test_grid.html'))
    # fig.show()


def plotly_us_census(df, color_var, labels, locations, hover_data=None, save_loc=None):
    """
    plotly for US census data
    :param dpath: DataFrame from census
    :param color_var: name of column to plot with color
    :param labels: name of columns for hovers
    :param locations: location id column in census dataframe
    """

    print('Running census map from API...')

    with urlopen('https://raw.githubusercontent.com/plotly/datasets/master/geojson-counties-fips.json') as response:
        counties = json.load(response)

    print(df.head())
    print(hover_data)

    fig = px.choropleth(df, geojson=counties, locations=locations, color=color_var,
                        title=f'US Census {color_var}',
                        color_discrete_map="Viridis",
                        scope="usa",
                        hover_name=df[labels],
                        # hover_data=df[color_var]
                        hover_data=df[list(hover_data.keys())],
                        labels=hover_data
                        )
    fig.update_layout(margin={"r": 0, "t": 0, "l": 0, "b": 0})
    plotly.offline.plot(fig, filename=str(save_loc / f'censusapi_{color_var}.html'))
    fig.show()


def plotly_us_census_api(apikey, tables, year, normalize, input, outdir):
    """
    Retrieve census data and create plotly object
    """

    df, meta_data = census_api.get_census_data(apikey, tables, year, normalize)

    # Zero pad FIPS
    df['FIPS'] = df['FIPS'].apply(lambda x: str(x).zfill(5))

    up_shot = read_df(Path(input) / 'CountyData.tsv')

    # Combine with upshot for posterity, and county names
    combined = up_shot.merge(df, left_on='id', right_on='FIPS')

    print(meta_data)

    plotly_us_census(combined, tables[0], labels='County', locations='FIPS', hover_data=meta_data, save_loc=outdir)

# --------------------------------------- Non-plotting ---------------------------------------------------#

def get_census_ts(apikey, tables, year, normalize, input, outdir):
    """

    :param apikey:
    :type apikey:
    :param tables:
    :type tables:
    :param year:
    :type year:
    :param normalize:
    :type normalize:
    :param input:
    :type input:
    :param outdir:
    :type outdir:
    :return:
    :rtype:
    """

    df, meta_data = census_api.get_census_data(apikey, tables, year, normalize)

    print(df.head())